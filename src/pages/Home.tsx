import { GROUPS } from "../api/groups/groupData";
import Group from "../components/group/group";
import Subject from "../components/subject/subject";
import groupSubjectState from "../store/groupSubjectState";
import groupSubjectList from "../store/groupSubjectList";
import { observer } from "mobx-react-lite";
import { subjectService } from "../api/subject/subjectService";
import { SubjectType } from "../api/subject/subjectType";
import { Link } from "react-router-dom";
import { useEffect, useState } from "react";
import Loader from "../components/loader/loader";
import { groupService } from "../api/groups/groupService";

const HomePage = observer(() => {
    const { group, subject } = groupSubjectState;
    const {
        groupList,
        setGroupList,
        subjectList,
        setSubject,
        currentSubject,
        setCurrentSubject,
    } = groupSubjectList;
    const [loadGroup, setLoadGroup] = useState(false);
    const [loadSubject, setLoadSubject] = useState(false);

    useEffect(() => {
        setLoadGroup(true);
        const promisGroup = groupService().getGroup();
        promisGroup
            .then((res) => setGroupList(res))
            .finally(() => {
                setLoadGroup(false);
            });
    }, []);

    useEffect(() => {
        setLoadSubject(true);
        if (group !== "") {
            const promisSubject = subjectService().getSubject(group);
            promisSubject
                .then((res) => setSubject(res))
                .finally(() => {
                    setLoadSubject(false);
                });
        }
    }, [group]);

    function dragOver(e: React.DragEvent<HTMLDivElement>) {
        e.preventDefault();
        if (
            e.target instanceof HTMLDivElement &&
            e.target.className === "item"
        ) {
            e.target.style.boxShadow = "0 2px 3px gray";
        }
    }

    function dragLeave(e: React.DragEvent<HTMLDivElement>) {
        if (e.target instanceof HTMLDivElement) {
            e.target.style.boxShadow = "none";
        }
    }

    function dragStart(e: React.DragEvent<HTMLDivElement>, item: SubjectType) {
        setCurrentSubject(item);
    }

    function dragEnd(e: React.DragEvent<HTMLDivElement>) {
        if (e.target instanceof HTMLDivElement) {
            e.target.style.boxShadow = "none";
        }
    }

    function drop(e: React.DragEvent<HTMLDivElement>, item: SubjectType) {
        e.preventDefault();
        if (e.target instanceof HTMLDivElement) {
            e.target.style.boxShadow = "none";
        }
        if (!currentSubject) return;

        const currentIndex = subjectList.findIndex(
            (el) => el.id === currentSubject.id
        );
        subjectList.splice(currentIndex, 1);
        const dropIndex = subjectList.findIndex((el) => el.id === item.id);
        subjectList.splice(dropIndex, 0, currentSubject);
        setSubject(subjectList.map((i) => (i.id === item.id ? item : i)));
    }
    const handleNoActive = () => {
        alert("Вы не выбрали группу или предмет");
    };

    return (
        <>
            <div className="boxHome">
                <div className="groupList">
                    <h2>Список групп:</h2>
                    {loadGroup ? (
                        <div className="homeLoadGroup">
                            <h2>Загрузка групп...</h2>
                            <Loader />
                        </div>
                    ) : (
                        <ul>
                            {groupList.map((el) => {
                                return (
                                    <Group
                                        key={el.id}
                                        id={el.id}
                                        name={el.name}
                                    />
                                );
                            })}
                        </ul>
                    )}
                </div>
                <div className="subjectsList">
                    <h2>Список предметов обучения:</h2>
                    <ul>
                        {group === "" ? (
                            <h3 className="homeLoad">Выберите группу</h3>
                        ) : loadSubject ? (
                            <div className="homeLoad">
                                <h2>Загрузка предметов для группы </h2>
                                <Loader />
                            </div>
                        ) : (
                            subjectList.map((el) => {
                                return (
                                    <div
                                        key={el.id}
                                        onDragOver={(e) => dragOver(e)}
                                        onDragLeave={(e) => dragLeave(e)}
                                        onDragStart={(e) => dragStart(e, el)}
                                        onDragEnd={(e) => dragEnd(e)}
                                        onDrop={(e) => drop(e, el)}
                                        className="item"
                                        draggable={true}
                                    >
                                        <Subject
                                            name={el.name}
                                            key={el.id}
                                            id={el.id}
                                            teachersName={el.teachersName}
                                            teachersLavel={el.teachersLavel}
                                            journalId={el.journalId}
                                        />
                                    </div>
                                );
                            })
                        )}
                    </ul>
                </div>
            </div>
            {group !== "" && subject !== "" ? (
                <Link to={`/group/${group}/subject/${subject}`} className="btn">
                    Отобразить посещаемость
                </Link>
            ) : (
                <div className="btn" onClick={handleNoActive}>
                    Отобразить посещаемость
                </div>
            )}
        </>
    );
});

export default HomePage;
