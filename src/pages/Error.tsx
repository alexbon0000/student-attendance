import { Link } from "react-router-dom";

const ErrorPage = () => {
    return (
        <div className="error">
            <h1>К сожалению, произошла непридвиденная ошибка.</h1>
            <Link to={"/"} className="btnError">
                Назад
            </Link>
        </div>
    );
};
export default ErrorPage;
