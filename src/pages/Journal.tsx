import { observer } from "mobx-react-lite";
import { journalService } from "../api/journal/journalService";
import JournalTable from "../components/journalTable/journalTable";
import journalState from "../store/journalState";
import { useEffect, useState } from "react";
import SearchStudent from "../components/searchStudent/searchStudent";
import input from "../store/input";
import Loader from "../components/loader/loader";
import { useParams, useNavigate } from "react-router-dom";

const JournalPage = observer(() => {
    const { journal, setJournal } = journalState;
    const { inputValue } = input;
    const [loading, setLoading] = useState(false);
    const { subjectId } = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        setLoading(true);
        const promisJornal = journalService().getJournal(subjectId || "");
        promisJornal
            .then((res) => setJournal(res))
            .catch(() => navigate("/error"))
            .finally(() => setLoading(false));
    }, []);
    const currentSubject = journal[0];

    const filterArrayByname = (
        searchString: string,
        originalArray: {
            id: number;
            fullName: string;
            attended: boolean[];
        }[]
    ) => {
        const filteredArray = originalArray.filter(
            (obj: { fullName: string }) => {
                const name = obj.fullName.toLowerCase();
                const search = searchString.toLowerCase();
                return name.includes(search);
            }
        );
        if (filteredArray.length > 0) {
            return filteredArray;
        } else return originalArray;
    };
    const filteredArray = filterArrayByname(
        inputValue,
        currentSubject.students || []
    );

    return (
        <>
            {loading ? (
                <div className="jornalLoad">
                    <h2>Загрузка данных о посещаемости</h2>
                    <Loader />
                </div>
            ) : (
                <>
                    <SearchStudent />
                    <h1>Информация о посещаемости</h1>
                    <div className="info">
                        Посещаемость занятий по предмету обучения{" "}
                        <span className="boldText">
                            {" "}
                            "{currentSubject.subjectName}"{" "}
                        </span>
                        студентами группы <br />
                        <span className="boldText">
                            "{currentSubject.groupName}"
                        </span>
                        <br /> Преподаватель{" "}
                        <span className="boldText">
                            {currentSubject.teachersName}
                        </span>
                    </div>
                    <JournalTable students={filteredArray} />
                </>
            )}
        </>
    );
});

export default JournalPage;
