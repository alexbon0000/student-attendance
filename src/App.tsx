import React from "react";
import HomePage from "./pages/Home";
import {
    Route,
    RouterProvider,
    createBrowserRouter,
    createRoutesFromElements,
} from "react-router-dom";
import ErrorPage from "./pages/Error";
import JournalPage from "./pages/Journal";
import { groupService } from "./api/groups/groupService";
function App() {
    const router = createBrowserRouter(
        createRoutesFromElements(
            <>
                <Route
                    path="/"
                    element={<HomePage />}
                    errorElement={<ErrorPage />}
                />
                <Route
                    path={"/group/:groupId/subject/:subjectId"}
                    element={<JournalPage />}
                />
                <Route path={"/error"} element={<ErrorPage />} />
                <Route path="*" element={<ErrorPage />} />
            </>
        )
    );
    return (
        <div className="container">
            <RouterProvider router={router} />
        </div>
    );
}

export default App;
