import { makeAutoObservable } from "mobx";
import { SubjectType } from "../api/subject/subjectType";
import { GroupType } from "../api/groups/groupType";

class GroupeSubjectList {
    groupList: GroupType[] = [
        {
            id: NaN,
            name: "",
        },
    ];
    subjectList: SubjectType[] = [
        {
            id: NaN,
            name: "",
            teachersName: "",
            teachersLavel: "",
            groupId: NaN,
            journalId: NaN,
        },
    ];
    currentSubject: SubjectType = {
        id: NaN,
        name: "",
        teachersName: "",
        teachersLavel: "",
        groupId: NaN,
        journalId: NaN,
    };
    constructor() {
        makeAutoObservable(this);
    }
    setSubject = (el: SubjectType[]) => {
        this.subjectList = el;
    };

    setCurrentSubject = (el: SubjectType) => {
        this.currentSubject = el;
    };
    setGroupList = (el: GroupType[]) => {
        this.groupList = el;
    };
}
export default new GroupeSubjectList();
