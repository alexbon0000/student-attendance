import { makeAutoObservable } from "mobx";

export class Load {
    loading: boolean = false;
    constructor() {
        makeAutoObservable(this);
    }
    setLoad = () => {
        this.loading = !this.loading;
    };
}
