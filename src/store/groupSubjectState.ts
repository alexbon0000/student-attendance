import { makeAutoObservable } from "mobx";

class GroupeSubjectState {
    group = "";
    subject = "";
    constructor() {
        makeAutoObservable(this);
    }
    setGroupe = (el: string) => {
        this.group = el;
    };
    setSubject = (el: string) => {
        this.subject = el;
    };
}
export default new GroupeSubjectState();
