import { makeAutoObservable } from "mobx";
import { JournalType } from "../api/journal/journalType";

class Journal {
    journal: JournalType[] = [
        {
            id: NaN,
            subjectId: NaN,
            teachersName: "",
            groupName: "",
            subjectName: "",
            students: [
                {
                    id: NaN,
                    fullName: "",
                    attended: [],
                },
            ],
        },
    ];
    constructor() {
        makeAutoObservable(this);
    }
    setJournal = (obj: JournalType[]) => {
        this.journal = obj;
    };
}
export default new Journal();
