import { makeAutoObservable } from "mobx";

class Input {
    inputValue: string = "";
    constructor() {
        makeAutoObservable(this);
    }
    setInputValue = (text: string) => {
        this.inputValue = text;
    };
    clearInput = () => {
        this.inputValue = "";
    };
}
export default new Input();
