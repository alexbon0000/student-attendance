import styles from "./attendance.module.scss";

const Attendance = (props: {
    id: number;
    fullName: string;
    attended: boolean[];
}) => {
    const attended = props.attended;
    const green = {
        backgroundColor: "green",
    };
    const red = {
        backgroundColor: "red",
    };
    return (
        <>
            <div className={styles.tableString}>
                <div className={styles.studentName}>{props.fullName}</div>
                {attended.map((el) => {
                    return (
                        <div
                            key={Math.random() * Math.random()}
                            style={el ? green : red}
                        >
                            {el ? "V" : "X"}
                        </div>
                    );
                })}
            </div>
        </>
    );
};

export default Attendance;
