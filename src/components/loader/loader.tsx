import styles from "./loader.module.scss";

const Loader = () => {
    return (
        <div className={styles.loader}>
            <img
                className={styles.loadImg}
                src="https://i.gifer.com/origin/34/34338d26023e5515f6cc8969aa027bca_w200.gif"
                alt="Загрузка"
            />
        </div>
    );
};

export default Loader;
