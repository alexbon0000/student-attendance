import { observer } from "mobx-react-lite";
import styles from "./group.module.scss";
import groupSubjectState from "../../store/groupSubjectState";

const Group = observer(
    (props: { path?: string; name: string; id?: number }) => {
        const { group, setGroupe } = groupSubjectState;
        const handleGroup = () => {
            setGroupe(props.id?.toString() || "");
        };

        const active = props.id?.toString() === group ? styles.active : "";
        return (
            <div onClick={handleGroup} className={styles.btn}>
                <div className={active}>{props.name}</div>
            </div>
        );
    }
);

export default Group;
