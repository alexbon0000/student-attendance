import { observer } from "mobx-react-lite";
import input from "../../store/input";
import styles from "./searchStudent.module.scss";

const SearchStudent = observer(() => {
    const { inputValue, setInputValue, clearInput } = input;

    const handleInputChange = (e: { target: { value: string } }) => {
        setInputValue(e.target.value);
    };

    return (
        <div className={styles.searchBox}>
            <input
                className={styles.searchInput}
                type="text"
                value={inputValue}
                placeholder="Ф.И.О. студента"
                onChange={handleInputChange}
            />
            <button className={styles.btn} onClick={() => clearInput()}>
                Сбросить
            </button>
        </div>
    );
});

export default SearchStudent;
