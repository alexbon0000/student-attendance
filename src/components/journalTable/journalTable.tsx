import Attendance from "../attendance/attendance";
import styles from "./table.module.scss";
import { observer } from "mobx-react-lite";

const JournalTable = observer(
    (props: {
        students: {
            id: number;
            fullName: string;
            attended: boolean[];
        }[];
    }) => {
        return (
            <>
                <div className={styles.table}>
                    <div className={styles.tableString}>
                        <div className={styles.studentName}>ФИО/Дата</div>
                        <div>1.09</div>
                        <div>2.09</div>
                        <div>3.09</div>
                        <div>4.09</div>
                        <div>5.09</div>
                        <div>6.09</div>
                        <div>7.09</div>
                        <div>8.09</div>
                        <div>9.09</div>
                        <div>10.09</div>
                        <div>11.09</div>
                        <div>12.09</div>
                        <div>13.09</div>
                        <div>14.09</div>
                        <div>15.09</div>
                    </div>
                    {props.students.length < 1
                        ? ""
                        : props.students.map((el) => {
                              return (
                                  <Attendance
                                      key={el.id}
                                      id={el.id}
                                      fullName={el.fullName}
                                      attended={el.attended}
                                  />
                              );
                          })}
                </div>
            </>
        );
    }
);

export default JournalTable;
