import { observer } from "mobx-react-lite";
import styles from "./subject.module.scss";
import groupSubjectState from "../../store/groupSubjectState";

const Subject = observer(
    (props: {
        id?: number;
        name: string;
        path?: string;
        teachersName?: string;
        journalId?: number;
        teachersLavel: string;
    }) => {
        const { subject, setSubject } = groupSubjectState;
        const active =
            props.journalId?.toString() === subject ? styles.active : "";
        const handleSubject = () => {
            setSubject(props.journalId?.toString() || "");
        };

        return (
            <div className={styles.btn} onClick={handleSubject}>
                <div className={active}>
                    {props.name} ({props.teachersLavel} {props.teachersName})
                </div>
            </div>
        );
    }
);

export default Subject;
