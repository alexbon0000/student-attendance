import { groupService } from "./../groups/groupService";
export type JournalType = {
    id: number;
    subjectId: number;
    teachersName: string;
    groupName: string;
    subjectName: string;
    students: {
        id: number;
        fullName: string;
        attended: boolean[];
    }[];
};
