import { JournalType } from "./journalType";
// prettier-ignore
export const JOURNAL: JournalType[] = [
    {
        id: 1,
        subjectId: 1,
        teachersName: "Иванов С.В.",
        groupName: "Группа 1",
        subjectName: "Предмет 1",
        students: [
            {
                id: 1,
                fullName: "Плюев С.Г.",
                attended: [true,true,false,true,true,false,true,false,false,true,false,true,true,true,false],
            },
            {
                id: 2,
                fullName: "Иванов Д.С.",
                attended: [false,true,false,true,true,true,true,false,true,true,false,true,false,false,true],
            },
            {
                id: 3,
                fullName: "Сидоров К.Ю.",
                attended: [true,true,true,true, true,true,true,false,true,true,false,true,false,false,true],
            },
        ],
    },
    {
        id: 2,
        subjectId: 2,
        teachersName: "Петров Д.Г.",
        groupName: "Группа 1",
        subjectName: "Предмет 2",
        students: [
            {
                id: 4,
                fullName: "Плюев С.Г.",
                attended: [true,true,false,true,true, true, true, false, true,true,true,true,false,true,true,],
            },
            {
                id: 5,
                fullName: "Иванов Д.С.",
                attended: [false,true,false,true,true,true, true, false,true,true,false,true,false,true,true],
            },
            {
                id: 6,
                fullName: "Сидоров К.Ю.",
                attended: [true,true,true,true,true,false,true,false,true,true,true, true,true,false,true],
            },
        ],
    },
    {
        id: 3,
        subjectId: 3,
        teachersName: "Сидоров М.С.",
        groupName: "Группа 1",
        subjectName: "Предмет 3",
        students: [
            {
                id: 4,
                fullName: "Плюев С.Г.",
                attended: [true,true,false,true,true,false,true,false, true,true,true,true,true,true,true],
            },
            {
                id: 5,
                fullName: "Иванов Д.С.",
                attended: [ false,true,false,true,true,true,true,false,true,true,false,true,false,true,true],
            },
            {
                id: 6,
                fullName: "Сидоров К.Ю.",
                attended: [true,true,true,true,true,false,true,true,true,true,false,true,true,false,true],
            },
        ],
    },
    {
        id: 4,
        subjectId: 4,
        teachersName: "Абрамов А.Г.",
        groupName: "Группа 1",
        subjectName: "Предмет 4",
        students: [
            {
                id: 4,
                fullName: "Плюев С.Г.",
                attended: [true, true, false, true, true, false, true, true, true, false, false, true, true, false, true],
            },
            {
                id: 5,
                fullName: "Иванов Д.С.",
                attended: [false, true, false, true, true, true, true, false, true, true, true, false, false, true, true],
            },
            {
                id: 6,
                fullName: "Сидоров К.Ю.",
                attended: [true, true, true, true, true, true, false, true, false, false, true, true, true, true, false],
            },
        ],
    },
    {
        id: 5,
        subjectId: 5,
        teachersName: "Иванов С.В.",
        groupName: "Группа 2",
        subjectName: "Предмет 1",
        students: [
            {
                id: 1,
                fullName: "Иванова С.А.",
                attended: [true, true, false, true, true, true, true,false, true, false, true,false, true, true,false],
            },
            {
                id: 2,
                fullName: "Сидоров А.А.",
                attended: [false, true, false, true, true,false, true, false, true,false, true, true,false, true, false],
            },
            {
                id: 3,
                fullName: "Лакин Д.В.",
                attended: [true, true, true, true, true, true,false, true, false, true,false, true, true,false, true],
            },
        ],
    },
    {
        id: 6,
        subjectId: 6,
        teachersName: "Петров Д.Г.",
        groupName: "Группа 2",
        subjectName: "Предмет 2",
        students: [
            {
                id: 4,
                fullName: "Иванова С.А.",
                attended: [true, true, false, true, true, true, true, true,false, true, false, true,false, true, true],
            },
            {
                id: 5,
                fullName: "Сидоров А.А.",
                attended: [false, true, false, true, true,true, true, false, true, true, true, true, true,false,false],
            },
            {
                id: 6,
                fullName: "Лакин Д.В.",
                attended: [true, true, true, true, true, true, false, true, true,true,false, true, false, true, true],
            },
        ],
    },
    {
        id: 7,
        subjectId: 7,
        teachersName: "Сидоров М.С.",
        groupName: "Группа 2",
        subjectName: "Предмет 3",
        students: [
            {
                id: 4,
                fullName: "Иванова С.А.",
                attended: [true, true, false, true, true,true, true, true, true, true, false, true, true,true, true,],
            },
            {
                id: 5,
                fullName: "Сидоров А.А.",
                attended: [false, true, false, true, true,false, true, false,true,false,false, true, false,true,false],
            },
            {
                id: 6,
                fullName: "Лакин Д.В.",
                attended: [true, true, true, true, true, true, true,false, true, false,true,false,false, true, false],
            },
        ],
    },
    {
        id: 8,
        subjectId: 8,
        teachersName: "Абрамов А.Г.",
        groupName: "Группа 2",
        subjectName: "Предмет 4",
        students: [
            {
                id: 4,
                fullName: "Иванова С.А.",
                attended: [true, true, false, true, true, true, true, true,false, true, false, true, true,false, true],
            },
            {
                id: 5,
                fullName: "Сидоров А.А.",
                attended: [false, true, false, true, true, true, true, true, true,false,true, true, false, true, true],
            },
            {
                id: 6,
                fullName: "Лакин Д.В.",
                attended: [true, true, true, true, true, false, true, true, true, true, true, true,false,true, true],
            },
        ],
    },
    {
        id: 9,
        subjectId: 9,
        teachersName: "Иванов С.В.",
        groupName: "Группа 3",
        subjectName: "Предмет 1",
        students: [
            {
                id: 1,
                fullName: "Обухов В.С.",
                attended: [true, true, false, true, true,true, true, true, true, true, false, true, true, true, true],
            },
            {
                id: 2,
                fullName: "Петрова А.И.",
                attended: [false, true, false, true, true, true, true, true, true,false, true, false, true, true,false],
            },
            {
                id: 3,
                fullName: "Пажкин И.А.",
                attended: [true, true, true, true, true,false, true, false, true,false, true, false, true, true, true],
            },
        ],
    },
    {
        id: 10,
        subjectId: 10,
        teachersName: "Петров Д.Г.",
        groupName: "Группа 3",
        subjectName: "Предмет 2",
        students: [
            {
                id: 4,
                fullName: "Обухов В.С.",
                attended: [true, true, false, true, true,true, true, false, true, true,false, true, false, true, true],
            },
            {
                id: 5,
                fullName: "Петрова А.И.",
                attended: [false, true, false, true, true,true, true, true, true, true,false, true, false, true, true],
            },
            {
                id: 6,
                fullName: "Пажкин И.А.",
                attended: [true, true, true, true, true,false, true, false, true, true,false, true, false, true, true],
            },
        ],
    },
    {
        id: 11,
        subjectId: 11,
        teachersName: "Сидоров М.С.",
        groupName: "Группа 3",
        subjectName: "Предмет 3",
        students: [
            {
                id: 4,
                fullName: "Обухов В.С.",
                attended: [true, true, false, true, true,false, true, false, true, true, true, true,false, true, false],
            },
            {
                id: 5,
                fullName: "Петрова А.И.",
                attended: [false, true, false, true, true, true, true,false, true, false, true, true,false, true, false],
            },
            {
                id: 6,
                fullName: "Пажкин И.А.",
                attended: [true, true, true, true, true, false, true, true, true, true, false, true, true, true, true],
            },
        ],
    },
    {
        id: 12,
        subjectId: 12,
        teachersName: "Абрамов А.Г.",
        groupName: "Группа 3",
        subjectName: "Предмет 4",
        students: [
            {
                id: 4,
                fullName: "Обухов В.С.",
                attended: [true, true, false, true, true, false, true, true, true, true, true, true, false, true, true],
            },
            {
                id: 5,
                fullName: "Петрова А.И.",
                attended: [false, true, false, true, true, true, true, false, true, true, true, true, false, true, true],
            },
            {
                id: 6,
                fullName: "Пажкин И.А.",
                attended: [true, true, true, true, true, true, true, true, true, false, true, true, true, true, false],
            },
        ],
    },
    {
        id: 13,
        subjectId: 13,
        teachersName: "Иванов С.В.",
        groupName: "Группа 4",
        subjectName: "Предмет 1",
        students: [
            {
                id: 1,
                fullName: "Антонова Е.П.",
                attended: [true, true, false, true, true, true, true, true, true, false, true, true, true, true, true],
            },
            {
                id: 2,
                fullName: "Васильев Д.А.",
                attended: [false, true, false, true, true, true, true, true, true, true,true, true, false, true, true],
            },
            {
                id: 3,
                fullName: "Норкин Н.В.",
                attended: [true, true, true, true, true, true, true, true, true, true,true, true, false, true, true],
            },
        ],
    },
    {
        id: 14,
        subjectId: 14,
        teachersName: "Петров Д.Г.",
        groupName: "Группа 4",
        subjectName: "Предмет 2",
        students: [
            {
                id: 4,
                fullName: "Антонова Е.П.",
                attended: [true, true, false, true, true,true, true, false, true, true,true, true, false, true, true],
            },
            {
                id: 5,
                fullName: "Васильев Д.А.",
                attended: [false, true, false, true, true, true,true, true, false, true, true,true, true, false, true],
            },
            {
                id: 6,
                fullName: "Норкин Н.В.",
                attended: [true, true, true, true, true,true, true, false, true, true,true, true, false, true, true],
            },
        ],
    },
    {
        id: 15,
        subjectId: 15,
        teachersName: "Сидоров М.С.",
        groupName: "Группа 4",
        subjectName: "Предмет 3",
        students: [
            {
                id: 4,
                fullName: "Антонова Е.П.",
                attended: [true, true, false, true, true,true, true, false, true, true, true,true, true, false, true],
            },
            {
                id: 5,
                fullName: "Васильев Д.А.",
                attended: [false, true, false, true, true, true, true,true, true, false, true, true,true, true, false],
            },
            {
                id: 6,
                fullName: "Норкин Н.В.",
                attended: [true, true, true, true, true,false, true, false, true, true,false, true, false, true, true],
            },
        ],
    },
    {
        id: 16,
        subjectId: 16,
        teachersName: "Абрамов А.Г.",
        groupName: "Группа 4",
        subjectName: "Предмет 4",
        students: [
            {
                id: 4,
                fullName: "Антонова Е.П.",
                attended: [true, true, false, true, true,false, true, false, true, true,false, true, false, true, true],
            },
            {
                id: 5,
                fullName: "Васильев Д.А.",
                attended: [false, true, false, true, true,false, true, false, true, true, true,false, true, false, true],
            },
            {
                id: 6,
                fullName: "Норкин Н.В.",
                attended: [true, true, true, true, true, true,false, true, false, true, true, true, true,false, true],
            },
        ],
    },
];
