import { sleep } from "../../utils/sleep";
import { JOURNAL } from "./journalData";
import { JournalType } from "./journalType";

export const journalService = () => {
    const getJournal = async (id: number | string) => {
        if (!id) throw new Error("oh dang!");
        let journals: JournalType[] = [];
        try {
            await sleep(500);
            journals = JOURNAL.filter(
                ({ subjectId }) => subjectId.toString() === id
            );
        } catch (error) {
            console.error("Ошибка получение групп");
            journals = [];
        }
        if (journals.length) return journals;
        throw new Error("oh dang!");
    };

    return { getJournal };
};
