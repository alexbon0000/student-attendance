import { sleep } from "../../utils/sleep";
import { GROUPS } from "./groupData";

export const groupService = () => {
    const getGroup = async () => {
        try {
            await sleep(500);
            return GROUPS;
        } catch (error) {
            console.error("Ошибка получение групп");
            return [];
        }
    };

    return { getGroup };
};
