import { GroupType } from "./groupType";

export const GROUPS: GroupType[] = [
    { id: 1, name: "Группа 1" },
    { id: 2, name: "Группа 2" },
    { id: 3, name: "Группа 3" },
    { id: 4, name: "Группа 4" },
];
