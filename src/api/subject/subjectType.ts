export type SubjectType = {
    id: number;
    groupId: number;
    name: string;
    teachersName: string;
    teachersLavel: string;
    journalId: number;
};
