import { sleep } from "../../utils/sleep";
import { SUBJECTS } from "./subjectData";
import { SubjectType } from "./subjectType";

export const subjectService = () => {
    const getSubject = async (id: number | string) => {
        if (!id) throw new Error("oh dang!");
        let subjects: SubjectType[] = [];
        try {
            await sleep(500);
            subjects = SUBJECTS.filter(
                ({ groupId }) => groupId.toString() === id
            );
        } catch (error) {
            console.error("Ошибка получение групп");
            subjects = [];
        }
        if (subjects.length) return subjects;
        throw new Error("oh dang!");
    };

    return { getSubject };
};
